package com.uxplore.mailimap.scheduler;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

//This class is not working with any module.
// It was written for testing purpose only
public class SendEmail implements Job {

    private String SMTP_HOST = "smtp.gmail.com";

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        this.sendTestingEmails();

    }


    public void sendTestingEmails() {
        String FROM_ADDRESS = "enter from address";
        String PASSWORD = "enter from address pwd";
        String FROM_NAME = "enter name";

        String[] recepients = {"to address 1", "to address 2"};
        String[] bccRecepients = {"bcc address"};

        try {
            Properties props = new Properties();
            props.put("mail.smtp.host", SMTP_HOST);
            props.put("mail.smtp.auth", "true");
            props.put("mail.debug", "false");
            props.put("mail.smtp.ssl.enable", "true");


            Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(FROM_ADDRESS, PASSWORD);
                }
            });

            Message msg = new MimeMessage(session);
            InternetAddress from = new InternetAddress(FROM_ADDRESS, FROM_NAME);
            msg.setFrom(from);
            //To Recipients
            InternetAddress[] toAddresses = new InternetAddress[recepients.length];
            for (int i = 0; i < recepients.length; i++) {
                toAddresses[i] = new InternetAddress(recepients[i]);
            }
            msg.setRecipients(Message.RecipientType.TO, toAddresses);
            //BCC Recipients
            InternetAddress[] bccAddresses = new InternetAddress[bccRecepients.length];
            for (int i = 0; i < bccRecepients.length; i++) {
                bccAddresses[i] = new InternetAddress(bccRecepients[i]);
            }
            msg.setRecipients(Message.RecipientType.BCC, bccAddresses);
            msg.setSubject("Imap Testing");
            msg.setContent("Uxplore Imap Under Testing", "text/plain");
            Transport.send(msg);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}




package com.uxplore.mailimap.RestController;

import com.uxplore.mailimap.DbConnections.EmailDataJdbc;
import com.uxplore.mailimap.model.AttachmentBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.Blob;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


/**
 * @author Ramesh Rao
 */

@RestController
public class DownloadAttachment {

    private static final Logger logger = LoggerFactory.getLogger(DownloadAttachment.class);


    @RequestMapping(value = {"/downloadAttachment/{id}/{tableName}"}, method = RequestMethod.GET)
    public @ResponseBody
    void downloadEmailAttachments(@PathVariable("id") int id, @PathVariable("tableName") String tableName, HttpServletResponse response) {
        logger.info("Download E-mail Attachments api triggered");
        EmailDataJdbc emailDataJdbc = new EmailDataJdbc();

        Map<String, Object> emailData = emailDataJdbc.getEmailData(id, tableName);

        Blob attachment = (Blob) emailData.get("email_attachment");
        emailData.remove("email_attachment");
        try {
            int blobLength = (int) attachment.length();
            byte[] blobAsBytes = attachment.getBytes(1, blobLength);
            attachment.free();
            ByteArrayInputStream byteStream = new ByteArrayInputStream(blobAsBytes);
            ObjectInputStream objStream = new ObjectInputStream(byteStream);
            List objList = (List) objStream.readObject();
            AttachmentBean attachmentBeanEmailDetails = this.createEmailDetailsFile(emailData);
            objList.add(attachmentBeanEmailDetails);
            byte[] zipArray = this.zipBytes(objList);
            response.setContentType("text/plain");
            response.setHeader("Content-Disposition", "attachment;filename=Email Attachments.zip");
            response.getOutputStream().write(zipArray);


        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //return response;
    }

    private AttachmentBean createEmailDetailsFile(Map<String, Object> emailData) {

        String fileContent = "email_from " + emailData.get("email_from") + "\n" +
                "email_to - " + emailData.get("email_to") + "\n" +
                "email_cc - " + emailData.get("email_cc") + "\n" +
                "email_bcc - " + emailData.get("email_bcc");

        byte[] byteFile = fileContent.getBytes();
        AttachmentBean attachmentBean = new AttachmentBean();
        attachmentBean.setFileName("EmailDetails.txt");
        attachmentBean.setFileContents(byteFile);
        return attachmentBean;
    }

    public byte[] zipBytes(List objList) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(baos);
        for (int d = 0; d < objList.size(); d++) {
            AttachmentBean attachmentBean = (AttachmentBean) objList.get(d);
            ZipEntry entry = new ZipEntry(attachmentBean.getFileName());
            entry.setSize(attachmentBean.getFileContents().length);
            zos.putNextEntry(entry);
            zos.write(attachmentBean.getFileContents());
            zos.closeEntry();
        }
        zos.close();
        return baos.toByteArray();
    }

}

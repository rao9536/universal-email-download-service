package com.uxplore.mailimap.controller;

import com.sun.mail.iap.ProtocolException;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.protocol.IMAPProtocol;
import com.uxplore.mailimap.DbConnections.EmailConfigJdbc;
import com.uxplore.mailimap.DbConnections.EmailDataJdbc;
import com.uxplore.mailimap.model.AppPropertyFields;
import com.uxplore.mailimap.model.AttachmentBean;
import com.uxplore.mailimap.model.EmailConfiguration;
import com.uxplore.mailimap.model.EmailDownloadData;
import io.micrometer.core.instrument.util.StringUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.mail.*;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.sql.rowset.serial.SerialBlob;
import java.io.*;
import java.lang.reflect.Method;
import java.sql.Blob;
import java.sql.Timestamp;
import java.util.*;

/**
 * @author Ramesh Rao
 */


public class CheckEmailController implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(CheckEmailController.class);

    private boolean threadTimeout = false;

    public EmailConfiguration emailConfiguration;

    public CheckEmailController() {
    }

    @Autowired
    public CheckEmailController(EmailConfiguration emailConfiguration) {
        this.emailConfiguration = emailConfiguration;
    }

    @Override
    public void run() {
        logger.info(Thread.currentThread().getName() + " thread is running");

        JSONObject mailConfigJson = new JSONObject(emailConfiguration.getMailConfig());
        String idle = mailConfigJson.getString("idle");

        // running independent threads
        if (idle.equalsIgnoreCase("true")) {
            this.imapIdleExtractEmail();
        } else {
            this.extractEmail();
        }
    }


    private void imapIdleExtractEmail() {

        logger.info("Thread is running in Idle Mode --- will auto sense new email !!");

        final long KEEP_ALIVE_FREQ = 1000;

        Message[] messages = null;
        try {
            Store store = this.getStore();

            IMAPFolder imapFolder = (IMAPFolder) store.getFolder(emailConfiguration.getMailServerMailbox());
            imapFolder.open(Folder.READ_ONLY);

            logger.info("Mail Box Opened!!");

            UIDFolder uf = (UIDFolder) imapFolder;

            if (StringUtils.isBlank(emailConfiguration.getLastUid())) {
                logger.info("getting messages from scratch !!");
                messages = imapFolder.getMessages();

            } else {
                logger.info("getting messages from last uid !!");
                String currentUid = emailConfiguration.getLastUid();
                long nextUid = Long.parseLong(currentUid);
                nextUid = nextUid + 1;

                messages = uf.getMessagesByUID(nextUid, UIDFolder.LASTUID);
            }

            String response = this.processAndSaveMsgs(messages, uf, null, imapFolder);

            imapFolder.addMessageCountListener(new EmailListener(emailConfiguration, store, uf, imapFolder));

            Thread currentThread = Thread.currentThread();
            int threadTimeoutValue = 0;
            threadTimeoutValue = AppPropertyFields.getThreadTimeoutValue(); // Time in minutes
            while (!currentThread.isInterrupted()) {
                if (threadTimeoutValue <= 0) {
                    currentThread.interrupt();
                    threadTimeout = true;
                    logger.info(" ******* ******* Entered Time Out function --- going for time out ****** *****");
                    break;
                } else {
                    threadTimeoutValue = threadTimeoutValue - 1;
                    int timeInMin = 60000;
                    while (timeInMin >= 0) {
                        try {
                            timeInMin = timeInMin - 1000;
                            Thread.sleep(KEEP_ALIVE_FREQ);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }

                try {
                    imapFolder.doCommand(new IMAPFolder.ProtocolCommand() {
                        public Object doCommand(IMAPProtocol p)
                                throws ProtocolException {
                            p.simpleCommand("NOOP", null);
                            return null;
                        }
                    });
                } catch (FolderClosedException fce) {
                    fce.printStackTrace();
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            }


            if (!threadTimeout) {
                Thread cThread = Thread.currentThread();
                while (!cThread.isInterrupted()) {
                    try {
                        if (!emailConfiguration.getMailServerHost().equalsIgnoreCase("imap.mail.yahoo.com"))
                            imapFolder.idle();
                    } catch (FolderClosedException e) {
                        logger.info("The remote server closed the IMAP folder, we're going to try reconnecting.");
                        this.imapIdleExtractEmail();
                    } catch (MessagingException e) {
                        logger.info("Now closing imap mailbox, due to unhandled exception:");
                        break;
                    }
                }
            }

        } catch (NoSuchProviderException e) {
            this.updateServerStatus(e.toString(), emailConfiguration);
            e.printStackTrace();
        } catch (MessagingException e) {
            this.updateServerStatus(e.toString(), emailConfiguration);
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void extractEmail() {
        Message[] messages = null;

        try {

            Store store = this.getStore();
            Folder inbox = store.getFolder(emailConfiguration.getMailServerMailbox());
            inbox.open(Folder.READ_ONLY);

            logger.info("Mail Box Opened!!");

            UIDFolder uf = (UIDFolder) inbox;
            if (StringUtils.isBlank(emailConfiguration.getLastUid())) {
                logger.info("getting messages from scratch !!");
                messages = inbox.getMessages();

            } else {
                logger.info("getting messages from last uid !!");
                String currentUid = emailConfiguration.getLastUid();
                long nextUid = (long) (Long.parseLong(currentUid) + 1);

                messages = uf.getMessagesByUID(nextUid, UIDFolder.LASTUID);
            }

            String response = this.processAndSaveMsgs(messages, uf, inbox, null);

            inbox.close(true);
            store.close();

        } catch (NoSuchProviderException e) {
            this.updateServerStatus(e.toString(), emailConfiguration);
            e.printStackTrace();
        } catch (MessagingException e) {
            this.updateServerStatus(e.toString(), emailConfiguration);
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private Store getStore() {

        Store store = null;

        try {
            logger.info("connecting to mail server !!");

            Properties props = new Properties();
            props.setProperty("mail.store.protocol", "imaps");
            if (emailConfiguration.isMailServerSsl()) {
                props.setProperty("mail.imap.ssl.enable", "true");
            }
            Session session = Session.getInstance(props, null);
            store = session.getStore("imaps");

            logger.info("email session done !!");

            store.connect(emailConfiguration.getMailServerHost(), emailConfiguration.getMailUserName(), emailConfiguration.getMailPassword());

            logger.info("connected with store !!");

            if (store.getFolder(emailConfiguration.getMailServerMailbox()) != null) {
                String serverStatus = "OK";
                this.updateServerStatus(serverStatus, emailConfiguration);
            }

        } catch (NoSuchProviderException e) {
            this.updateServerStatus(e.toString(), emailConfiguration);
            e.printStackTrace();
        } catch (MessagingException e) {
            this.updateServerStatus(e.toString(), emailConfiguration);
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return store;
    }

    public String processAndSaveMsgs(Message[] messages, UIDFolder uf, Folder inbox, IMAPFolder imapFolder) {

        logger.info("No of Messages : " + messages.length);

        try {
            long lastUid = 0;
            for (int i = 0; i <= messages.length - 1; i++) {
                EmailDownloadData emailDownloadData = new EmailDownloadData();
                Message mailMsg = messages[i];


                Long uid = uf.getUID(mailMsg);
                if (uid > lastUid) {
                    lastUid = uid;
                }

                // escaping last one duplicate msg
                if (emailConfiguration.getLastUid() != null && (uid.equals(Long.parseLong(emailConfiguration.getLastUid())))) {
                    continue;
                }


                Address[] fromAddress = mailMsg.getFrom();
                String emailFrom = fromAddress[0].toString();
                String emailSubject = mailMsg.getSubject();
                String emailToList = this.parseAddresses(mailMsg
                        .getRecipients(Message.RecipientType.TO));
                String emailCcList = this.parseAddresses(mailMsg
                        .getRecipients(Message.RecipientType.CC));
                String emailBccList = this.parseAddresses(mailMsg
                        .getRecipients(Message.RecipientType.BCC));

                String emailMessageBody = this.getTextFromMessage(mailMsg);
                String emailMessageHtmlBody = this.getBodyHtmlFrmMsg(mailMsg);
                java.util.Date utilDate = new java.util.Date();
                java.sql.Timestamp createDate = new java.sql.Timestamp(utilDate.getTime());

                Date emailReceivedDate = mailMsg.getReceivedDate();

                java.sql.Timestamp emailReceivedTimeStamp = new java.sql.Timestamp(emailReceivedDate.getTime());
                String emailHeaders = this.getHeaders(mailMsg);
                String emailFlags = this.getAllFlags(mailMsg);
                String emailMsgUID;
                if (imapFolder != null) {
                    emailMsgUID = this.getMessageUIDByIMAPFolder(imapFolder, mailMsg);
                } else {
                    emailMsgUID = this.getMessageUID(inbox, mailMsg);
                }

                emailDownloadData.setEmailFrom(emailFrom);
                emailDownloadData.setEmailSubject(emailSubject);
                emailDownloadData.setEmailTo(emailToList);
                emailDownloadData.setEmailCC(emailCcList);
                emailDownloadData.setEmailBCC(emailBccList);
                emailDownloadData.setEmailDateReceived(emailReceivedTimeStamp);
                emailDownloadData.setEmailBody(emailMessageBody);
                emailDownloadData.setEmailBodyHtml(emailMessageHtmlBody);

                //String emailAttachmentType = this.getAttachmentType(mailMsg);
                List emailAttachmentsDataList = this.getAttachments(mailMsg);
                Blob emailAttachmentsBuf = new SerialBlob((byte[]) emailAttachmentsDataList.get(0));
                emailDownloadData.setEmailAttachmentType((String)emailAttachmentsDataList.get(1));
                emailDownloadData.setEmailAttachment(emailAttachmentsBuf);
                emailDownloadData.setEmailHeaders(emailHeaders);
                emailDownloadData.setEmailFlags(emailFlags);
                emailDownloadData.setEmailUID(emailMsgUID);

                emailDownloadData.setDateCreated(createDate);
                emailDownloadData.setDateUpdated(null);
                emailDownloadData.setDateCompleted(null);
                emailDownloadData.setStatus(null);
                emailDownloadData.setTimeTaken(0);

                EmailDataJdbc emailDataJdbc = new EmailDataJdbc();
                emailDataJdbc.saveEmailData(emailDownloadData, emailConfiguration);

                this.updateDataEmailConfig(lastUid, emailConfiguration.getMailUserName(),emailReceivedTimeStamp);
            }



        } catch (NoSuchProviderException e) {
            this.updateServerStatus(e.toString(), emailConfiguration);
            e.printStackTrace();
        } catch (MessagingException e) {
            this.updateServerStatus(e.toString(), emailConfiguration);
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "email saved successfully";
    }


    public void updateServerStatus(String status, EmailConfiguration emailConfiguration) {
        EmailConfigJdbc emailConfigJdbc = new EmailConfigJdbc();
        emailConfigJdbc.updateServerStatus(status, emailConfiguration);
    }

    private void updateDataEmailConfig(long lastUid, String mailUserName, Timestamp emailReceivedDate) {

        EmailConfigJdbc emailConfigJdbc = new EmailConfigJdbc();
        emailConfigJdbc.updateDataEmailConfig(lastUid, mailUserName,emailReceivedDate);

    }

    private String getMessageUIDByIMAPFolder(IMAPFolder folder, Message message) {

        String uid = null;
        if (folder instanceof IMAPFolder) {
            try {
                uid = Long.toString(((IMAPFolder) folder).getUID(message));
            } catch (NoSuchElementException exception) {
                exception.printStackTrace();
            } catch (MessagingException ignore) {
                ignore.printStackTrace();
            }
        } else {
            try {
                Method m = folder.getClass().getMethod(
                        "getUID", Message.class);
                Object o = m.invoke(folder, new Object[]{message});
                if (o != null && o instanceof Long) {
                    uid = Long.toString((Long) o);
                } else if (o != null && o instanceof String) {
                    uid = (String) o;
                }
            } catch (Exception ignore) {
                ignore.printStackTrace();
            }
        }
        return uid;
    }

    private String getMessageUID(Folder folder, Message message) {
        String uid = null;
        if (folder instanceof UIDFolder) {
            try {
                uid = Long.toString(((UIDFolder) folder).getUID(message));
            } catch (NoSuchElementException exception) {
                exception.printStackTrace();
            } catch (MessagingException ignore) {
                ignore.printStackTrace();
            }
        } else {
            try {
                Method m = folder.getClass().getMethod(
                        "getUID", Message.class);
                Object o = m.invoke(folder, new Object[]{message});
                if (o != null && o instanceof Long) {
                    uid = Long.toString((Long) o);
                } else if (o != null && o instanceof String) {
                    uid = (String) o;
                }
            } catch (Exception ignore) {
                ignore.printStackTrace();
            }
        }
        return uid;
    }


    private String getAllFlags(Message mailMsg) {
        String flags = "";
        try {
            Flags allFlags = mailMsg.getFlags();
            String[] userFlags = allFlags.getUserFlags();
            for (int i = 0; i <= userFlags.length - 1; i++) {
                flags += userFlags[i] + ",";
            }

            Flags.Flag[] systemFlags = allFlags.getSystemFlags();
            for (int i = 0; i <= systemFlags.length - 1; i++) {
                Flags.Flag flag = systemFlags[i];
                if (flag == Flags.Flag.SEEN) {
                    flags += "Read " + ", ";
                } else {
                    flags += "Unread " + ", ";
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return flags;
    }


    private String parseAddresses(Address[] address) {
        logger.info("Parsing addresses -- parseAddresses(Address[] address)");
        String listAddress = "";

        if (address != null) {
            for (int i = 0; i < address.length; i++) {
                listAddress += address[i].toString() + ",";
            }
        }
        if (listAddress.length() > 1) {
            listAddress = listAddress.substring(0, listAddress.length() - 1);
        }

        return listAddress;
    }


    private String getTextFromMessage(Message message) throws MessagingException, IOException {

        logger.info("getting body text -- getTextFromMessage(Message message)");

        String result = "";
        if (message.isMimeType("text/plain")) {
            result = message.getContent().toString();
        } else if (message.isMimeType("multipart/*")) {
            MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
            result = getTextFromMimeMultipart(mimeMultipart);
        }
        return result;
    }


    private String getTextFromMimeMultipart(
            MimeMultipart mimeMultipart) throws MessagingException, IOException {

        logger.info("getting body text from html/plain -- getTextFromMimeMultipart()");

        String result = "";
        int count = mimeMultipart.getCount();
        for (int i = 0; i < count; i++) {
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            if (bodyPart.isMimeType("text/plain")) {
                result = result + "\n" + bodyPart.getContent();
                break; // without break same text appears twice in my tests
            } else if (bodyPart.isMimeType("text/html")) {
                String html = (String) bodyPart.getContent();
                result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
            } else if (bodyPart.getContent() instanceof MimeMultipart) {
                result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
            }
        }
        return result;
    }

    private String getBodyHtmlFrmMsg(Message mailMsg) throws MessagingException, IOException {

        logger.info("getting body html -- getBodyHtmlFrmMsg(Message mailMsg)");

        String result = "";

        if (mailMsg.isMimeType("multipart/*")) {
            MimeMultipart mimeMultipart = (MimeMultipart) mailMsg.getContent();
            result = getHtmlFromMimeMultipart(mimeMultipart);
        }

        return result;
    }


    private String getHtmlFromMimeMultipart(
            MimeMultipart mimeMultipart) throws MessagingException, IOException {

        logger.info("getting body html -- getHtmlFromMimeMultipart()");

        String result = "";
        int count = mimeMultipart.getCount();
        for (int i = 0; i < count; i++) {
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
                 if (bodyPart.isMimeType("text/html")) {
                     result = result + bodyPart.getContent();
            } else if (bodyPart.getContent() instanceof MimeMultipart) {
                result = result + getHtmlFromMimeMultipart((MimeMultipart) bodyPart.getContent());
            }
        }
        return result;
    }

    /*private String getAttachmentType(Message message) {

        logger.info("getting attachment types -- getAttachmentType()");

        String attachmentName = "";
        try {
            String contentType = message.getContentType();

            if (contentType.contains("multipart")) {
                Multipart multiPart = (Multipart) message.getContent();
                int numberOfParts = multiPart.getCount();
                for (int partCount = 0; partCount < numberOfParts; partCount++) {
                    MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(partCount);
                    if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
                        String fileName = part.getFileName();
                        attachmentName += fileName + ",";
                    }
                }
                if (attachmentName.length() > 1) {
                    attachmentName = attachmentName.substring(0, attachmentName.length() - 1);
                }

            }

        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return attachmentName;

    }*/

    private List getAttachments(Message mailMsg) {

        logger.info("getting attachments -- getAttachments()");

        List finalList = new ArrayList();



        byte[] attachments = null;

        String attachmentName = "";
        List<AttachmentBean> attachmentBeanList = new ArrayList<>();
        try {
            Object content = mailMsg.getContent();
            if (content instanceof String) {
                String body = (String) content;
                attachments = body.getBytes();
            } else if (content instanceof Multipart) {
                Multipart multipart = (Multipart) mailMsg.getContent();
                for (int i = 0; i < multipart.getCount(); i++) {
                    byte[] buff = null;
                    BodyPart bodyPart = multipart.getBodyPart(i);
                    Part part = multipart.getBodyPart(i);
                    String fileName;
                    if (!Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition()) &&
                            StringUtils.isBlank(bodyPart.getFileName())) {

                        attachmentBeanList = this.getInlineAttachments(bodyPart);
                        for (int k=0; k<attachmentBeanList.size();k++){
                            AttachmentBean attachmentBean = attachmentBeanList.get(k);
                            fileName = attachmentBean.getFileName();
                            attachmentName += fileName + ",";
                        }
                        continue;
                    }

                    fileName = bodyPart.getFileName();
                    attachmentName += fileName + ",";
                    AttachmentBean attachmentBean = new AttachmentBean();
                    InputStream is = part.getInputStream();
                    buff = IOUtils.toByteArray(is);
                    attachmentBean.setFileName(fileName);
                    attachmentBean.setFileContents(buff);

                    attachmentBeanList.add(attachmentBean);
                }

                attachments = this.ConvertIntoByteArray(attachmentBeanList);
                logger.info("All attachemnts in e-mail size is : " + attachments.length);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        if (attachmentName.length() > 1) {
            attachmentName = attachmentName.substring(0, attachmentName.length() - 1);
        }

        finalList.add(attachments);
        finalList.add(attachmentName);

        return finalList;
    }

    private List getInlineAttachments(BodyPart bodyPart){
        List<AttachmentBean> attachmentBeanList = new ArrayList<>();
        try {
            String fileName="";
            InputStream is;
            Part part;
            byte[] buff = null;
            Object content = bodyPart.getContent();
            if(content instanceof Multipart) {
                Multipart inLinemultiPart = (Multipart) bodyPart.getContent();
                int numberOfInLineParts = inLinemultiPart.getCount();
                for (int partCount = 0; partCount < numberOfInLineParts; partCount++) {
                    MimeBodyPart inLinemimeBodyPart = (MimeBodyPart) inLinemultiPart.getBodyPart(partCount);
                    String inLinedisposition = inLinemimeBodyPart.getDisposition();

                    if (inLinedisposition != null && Part.INLINE.equalsIgnoreCase(inLinedisposition)) {
                        AttachmentBean attachmentBean = new AttachmentBean();
                        fileName = inLinemimeBodyPart.getFileName();
                        part = inLinemultiPart.getBodyPart(partCount);
                        is = part.getInputStream();
                        buff = IOUtils.toByteArray(is);

                        attachmentBean.setFileName(fileName);
                        attachmentBean.setFileContents(buff);

                        attachmentBeanList.add(attachmentBean);

                    }
                }
            }
        }catch (IOException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return attachmentBeanList;
    }


    private byte[] ConvertIntoByteArray(List<AttachmentBean> attachmentBeanList) {

        byte[] allBytes = null;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(bos);
            os.writeObject(attachmentBeanList);
            allBytes = bos.toByteArray();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return allBytes;
    }

    private String getHeaders(Message mailMsg) {

        logger.info("getting headers -- getHeaders()");

        String header = "";

        try {
            Enumeration headers = mailMsg.getAllHeaders();
            while (headers.hasMoreElements()) {
                Header hd = (Header) headers.nextElement();
                header += hd.getName() + ": " + hd.getValue() + ",";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return header;
    }


}
package com.uxplore.mailimap.model;


import java.io.Serializable;

/**
 * @author Ramesh Rao
 */

public class AttachmentBean implements Serializable, Cloneable {
    private String fileName;
    private byte[] fileContents;


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getFileContents() {
        return fileContents;
    }

    public void setFileContents(byte[] fileContents) {
        this.fileContents = fileContents;
    }
}

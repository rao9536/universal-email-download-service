package com.uxplore.mailimap.DbConnections;

import com.uxplore.mailimap.Utils.Util;
import com.uxplore.mailimap.model.AppPropertyFields;
import com.uxplore.mailimap.model.EmailConfiguration;
import com.uxplore.mailimap.model.EmailDownloadData;
import io.micrometer.core.instrument.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 * @author Ramesh Rao
 */


public class EmailDataJdbc {

    private static final Logger logger = LoggerFactory.getLogger(EmailConfigJdbc.class);

    static final String TIME_ZONE = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";


    public void saveEmailData(EmailDownloadData emailDownloadData, EmailConfiguration emailConfiguration) {

        Connection conn = null;
        PreparedStatement pstmt = null;


        try {
            Class.forName(AppPropertyFields.getJdbcDriver());
            logger.info("Connecting to a selected database -- saveEmailData()");
            conn = DriverManager.getConnection(AppPropertyFields.getDbUrl() + AppPropertyFields.getDbName()+TIME_ZONE, AppPropertyFields.getDbUser(), AppPropertyFields.getDbPwd());
            logger.info("Connected database successfully -- saveEmailData()");

                logger.info("Creating statement -- saveEmailData()");
                pstmt = conn.prepareStatement("insert into "+emailConfiguration.getMailDbDatabase()+"." + emailConfiguration.getMailDbTable() + " (`email_uid`,`email_from`,`email_to`," +
                        "`email_cc`,`email_bcc`,`email_subject`,`email_body`,email_attachment,`email_attachment_types`,email_date_received," +
                        "`email_headers`,`email_flags`,`email_labels`,date_created,date_updated,date_completed,`status`,time_taken,`email_body_html`)  " +
                        "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);

                pstmt.setString(1, emailDownloadData.getEmailUID());
                pstmt.setString(2, emailDownloadData.getEmailFrom());
                pstmt.setString(3, emailDownloadData.getEmailTo());
                pstmt.setString(4, emailDownloadData.getEmailCC());
                pstmt.setString(5, emailDownloadData.getEmailBCC());
                pstmt.setString(6, emailDownloadData.getEmailSubject());
                pstmt.setString(7, emailDownloadData.getEmailBody());
                pstmt.setBlob(8, emailDownloadData.getEmailAttachment());
                pstmt.setString(9, emailDownloadData.getEmailAttachmentType());
                pstmt.setTimestamp(10, emailDownloadData.getEmailDateReceived());
                pstmt.setString(11, emailDownloadData.getEmailHeaders());
                pstmt.setString(12, emailDownloadData.getEmailFlags());
                pstmt.setString(13, emailDownloadData.getEmailLabels());
                pstmt.setTimestamp(14, emailDownloadData.getDateCreated());
                pstmt.setTimestamp(15, emailDownloadData.getDateUpdated());
                pstmt.setTimestamp(16, emailDownloadData.getDateCompleted());
                pstmt.setString(17, emailDownloadData.getStatus());
                pstmt.setLong(18, emailDownloadData.getTimeTaken());
                pstmt.setString(19, emailDownloadData.getEmailBodyHtml());

                pstmt.executeUpdate();
                logger.info("Executing statement -- saveEmailData()");
                int id = 0;
                ResultSet rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    id = rs.getInt(1);
                }

                logger.info("Executing statement -- Inserted row Id " + id);

                String postdataQuery = emailConfiguration.getMailDbPostQuery();
                if(!StringUtils.isBlank(postdataQuery)) {
                    CallableStatement cstmt = conn.prepareCall(postdataQuery);
                    cstmt.setInt(1, id);
                    cstmt.executeQuery();
                    logger.info("Executed MailDbPostQuery statement -- saveEmailData()");
                }

            if(!StringUtils.isBlank(emailConfiguration.getBashExecution())) {
                logger.info("Entered into attachment saving on server path");
                Blob attachment = (Blob) emailDownloadData.getEmailAttachment();
                int blobLength = (int) attachment.length();
                byte[] blobAsBytes = attachment.getBytes(1, blobLength);
                attachment.free();
                ByteArrayInputStream byteStream = new ByteArrayInputStream(blobAsBytes);
                ObjectInputStream objStream = new ObjectInputStream(byteStream);
                List objList = (List) objStream.readObject();

                Util util = new Util();

                String dirPath = util.createFileAttachment(emailDownloadData.getEmailBody(),objList,id,emailConfiguration);

                util.processAttachments(emailConfiguration,dirPath,id);

            }

            conn.close();

        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    conn.close();
            } catch (SQLException se) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

    }


    public Map getEmailData(int id, String tableName) {
        Map dataMap = new HashMap();

        Connection conn = null;
        Statement stmt = null;
        List<EmailConfiguration> emailConfigurationList = new ArrayList();
        try {
            Class.forName(AppPropertyFields.getJdbcDriver());

            logger.info("Connecting to a selected database -- getEmailData()");

            conn = DriverManager.getConnection(AppPropertyFields.getDbUrl() + AppPropertyFields.getDbName(), AppPropertyFields.getDbUser(), AppPropertyFields.getDbPwd());

            logger.info("Connected database successfully -- getEmailData()");


            logger.info("Creating statement -- getEmailData()");
            stmt = conn.createStatement();

            StringBuffer selectDataQuery = new StringBuffer("select email_from,email_to,email_cc,email_bcc,email_attachment from ").
                    append(tableName).append(" where id=").append(id);


            ResultSet rs = stmt.executeQuery(selectDataQuery.toString());

            logger.info("Statement executed successfully -- getEmailData()");

            while (rs.next()) {

                dataMap.put("email_from",rs.getString("email_from"));
                dataMap.put("email_to",rs.getString("email_to"));
                dataMap.put("email_cc",rs.getString("email_cc"));
                dataMap.put("email_bcc",rs.getString("email_bcc"));
                dataMap.put("email_attachment",rs.getBlob("email_attachment"));
            }

            rs.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException se) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

            return dataMap;
    }
}

package com.uxplore.mailimap.scheduler;

import com.uxplore.mailimap.controller.EmailConfigurationController;
import com.uxplore.mailimap.model.AppPropertyFields;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

public class EmailDownloadScheduler {

    public void runEmailsScheduler() {
        try {
            JobDetail job = JobBuilder.newJob(EmailConfigurationController.class)
                    .withIdentity("job", "group").build();

            Trigger trigger = TriggerBuilder.newTrigger()
                    .withIdentity("cronTrigger", "group")
                    .withSchedule(CronScheduleBuilder.cronSchedule(AppPropertyFields.getThreadTime()))
                    .build();

            Scheduler scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.start();
            scheduler.scheduleJob(job, trigger);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

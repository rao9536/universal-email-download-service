package com.uxplore.mailimap.Utils;

import com.uxplore.mailimap.model.AppPropertyFields;
import com.uxplore.mailimap.model.AttachmentBean;
import com.uxplore.mailimap.model.EmailConfiguration;
import io.micrometer.core.instrument.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;

public class Util {

    private static final Logger logger = LoggerFactory.getLogger(Util.class);

    public String createFileAttachment(String emailBody, List objList, int id, EmailConfiguration emailConfiguration) {

        logger.info("saving attachment on server path -- createFileAttachment()");
        File dir=null;

        try {
            boolean isWindows = System.getProperty("os.name")
                    .toLowerCase().startsWith("windows");

            if(isWindows){
                logger.info("Windows system detected in attachment saving");
                dir = new File("D:\\"+emailConfiguration.getMailDbTable()+"_"+id);
            }else{
                logger.info("Linux system detected in attachment saving");
                dir = new File(AppPropertyFields.getAttachmentsPath()+"/"+emailConfiguration.getMailDbTable()+"_"+id);
            }


            if (!dir.exists()){
                dir.mkdir();
            }else{
                logger.info("Directory already exists");
            }

            for (int d = 0; d < objList.size(); d++) {
                logger.info("Writing attachment -- createFileAttachment()");
                AttachmentBean attachmentBean = (AttachmentBean) objList.get(d);
                File file = new File(dir.getPath(), attachmentBean.getFileName());
                OutputStream os = new FileOutputStream(file);

                // creating file from buff
                os.write(attachmentBean.getFileContents());
                os.close();
            }

            if(!StringUtils.isBlank(emailBody)){
                logger.info("Creating body text file -- createFileAttachment()");
                String textFileName = "Uxplore_EmailBody.txt";
                File textFile = new File(dir.getPath(), textFileName);
                PrintWriter writer = new PrintWriter(textFile);
                writer.print(emailBody);
                writer.close();

            }

        }catch (Exception e) {
            e.printStackTrace();
        }
        return dir.getAbsolutePath();
    }

    public void processAttachments(EmailConfiguration emailConfiguration, String dirPath,int rowEmailId) {

        try{
            //Runtime r = Runtime.getRuntime();
            String filePath = emailConfiguration.getBashExecution();

            boolean isWindows = System.getProperty("os.name")
                    .toLowerCase().startsWith("windows");
            Process process;
            ProcessBuilder pb = new ProcessBuilder(filePath,dirPath,String.valueOf(rowEmailId));

            if(isWindows){
                logger.info("Windows system detected in attachment parsing");
                //String[] winCommand = {"bash", "-c", filePath};
                //process = r.exec(winCommand);
                process=pb.start();
                process.waitFor();
            }else{
                logger.info("Linux system detected in attachment parsing");
                //String[] linuxCommand = {"/bin/bash", filePath};
                //process = r.exec(linuxCommand);
                process=pb.start();
                process.waitFor();
            }

            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));


            String line = "";
            while ((line = reader.readLine()) != null) {
                logger.info("Bash scripts execution done "+line);
            }

            line = "";
            while ((line = errorReader.readLine()) != null) {
                logger.info("Bash scripts execution done with error "+line);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}

package com.uxplore.mailimap.DbConnections;

import com.uxplore.mailimap.model.AppPropertyFields;
import com.uxplore.mailimap.model.EmailConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.*;

/**
 * @author Ramesh Rao
 */


public class EmailConfigJdbc {

    private static final Logger logger = LoggerFactory.getLogger(EmailConfigJdbc.class);

    static final String CREATE_TABLE_QUERY = "(id bigint NOT NULL AUTO_INCREMENT, email_uid varchar(300),email_from varchar(300),\n" +
            "    email_to varchar(300), email_cc varchar(300), email_bcc varchar(300), email_subject varchar(300), email_body text, email_body_html text,\n" +
            "    email_attachment LONGBLOB, email_attachment_types varchar(300), email_date_received datetime, email_headers text,\n" +
            "    email_flags varchar(300), email_labels varchar(300), date_created datetime, date_updated datetime,\n" +
            "    date_completed datetime, status varchar(300), time_taken bigint, PRIMARY KEY (id)) ;";


    static final String GET_HEALTH_STATUS_QUERY = "Select mail_username,mail_config,last_uid,last_fetch_date,mail_db_post_query,mail_server_response_status," +
            "mail_server_connection_date,date_created,date_updated,status from Email_Config";

    static final String SELECT_TABLE_QUERY = "Select * from Email_Config where status='ACTIVE'";

    static final String TIME_ZONE = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";



    public List getEmailConfigData() {
        Connection conn = null;
        Statement stmt = null;
        List<EmailConfiguration> emailConfigurationList = new ArrayList();
        try {
            Class.forName(AppPropertyFields.getJdbcDriver());

            logger.info("Connecting to a selected database -- getEmailConfigData()");

            conn = DriverManager.getConnection(AppPropertyFields.getDbUrl() + AppPropertyFields.getDbName()+TIME_ZONE, AppPropertyFields.getDbUser(), AppPropertyFields.getDbPwd());

            logger.info("Connected database successfully -- getEmailConfigData()");


            logger.info("Creating statement -- getEmailConfigData()");
            stmt = conn.createStatement();

            ResultSet rs = stmt.executeQuery(SELECT_TABLE_QUERY);

            logger.info("Statement executed successfully -- getEmailConfigData()");

            while (rs.next()) {

                EmailConfiguration emailConfiguration = new EmailConfiguration();

                //Retrieve by column name
                emailConfiguration.setAccountName(rs.getString("account_name"));
                emailConfiguration.setMailServerHost(rs.getString("mail_server_host"));
                emailConfiguration.setMailServerPort(rs.getInt("mail_server_port"));
                emailConfiguration.setMailServerSsl(rs.getBoolean("mail_server_ssl"));
                emailConfiguration.setMailServerMailbox(rs.getString("mail_server_mailbox"));
                emailConfiguration.setMailUserName(rs.getString("mail_username"));
                emailConfiguration.setMailPassword(rs.getString("mail_password"));
                emailConfiguration.setMailConfig(rs.getString("mail_config"));
                emailConfiguration.setLastUid(rs.getString("last_uid"));
                emailConfiguration.setLastFetchDate(rs.getDate("last_fetch_date"));
                emailConfiguration.setMailDbDatabase(rs.getString("mail_db_database"));
                emailConfiguration.setMailDbTable(rs.getString("mail_db_table"));
                emailConfiguration.setMailDbPostQuery(rs.getString("mail_db_post_query"));
                emailConfiguration.setMailServerResponseStatus(rs.getString("mail_server_response_status"));
                emailConfiguration.setMailServerConnectionDate(rs.getDate("mail_server_connection_date"));
                emailConfiguration.setMetadata(rs.getString("metadata"));
                emailConfiguration.setDateCreated(rs.getDate("date_created"));
                emailConfiguration.setDateUpdated(rs.getDate("date_updated"));
                emailConfiguration.setStatus(rs.getString("status"));
                emailConfiguration.setDateCompleted(rs.getDate("date_completed"));
                emailConfiguration.setTimeTaken(rs.getInt("time_taken"));
                emailConfiguration.setBashExecution(rs.getString("bash_execution"));


                // creating table
                this.createEmailTable(rs.getString("mail_db_table"), emailConfiguration);

                emailConfigurationList.add(emailConfiguration);

            }
            rs.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException se) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

        return emailConfigurationList;
    }

    private void createEmailTable(String tableName, EmailConfiguration emailConfiguration) {

        StringBuffer createTableQuery = new StringBuffer("create table IF NOT EXISTS ").append(tableName).append(CREATE_TABLE_QUERY);
        Connection con = null;
        Statement stm = null;
        try {
            Class.forName(AppPropertyFields.getJdbcDriver());
            String dbUrl = AppPropertyFields.getDbUrl();
            dbUrl.trim();
            con = DriverManager.getConnection(AppPropertyFields.getDbUrl() + emailConfiguration.getMailDbDatabase()+TIME_ZONE,
                    AppPropertyFields.getDbUser(), AppPropertyFields.getDbPwd());
            stm = con.createStatement();
            stm.execute(createTableQuery.toString());
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (stm != null)
                    con.close();
            } catch (SQLException se) {
            }
            try {
                if (con != null)
                    con.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    public void updateDataEmailConfig(long lastUid, String mailUserName,Timestamp emailReceivedDate) {

        Connection conn = null;
        PreparedStatement pstmt = null;

        try {
            Class.forName(AppPropertyFields.getJdbcDriver());

            logger.info("Connecting to a selected database -- updateDataEmailConfig()");
            conn = DriverManager.getConnection(AppPropertyFields.getDbUrl() + AppPropertyFields.getDbName()+TIME_ZONE, AppPropertyFields.getDbUser(), AppPropertyFields.getDbPwd());
            logger.info("Connected database successfully -- updateDataEmailConfig()");
            pstmt = conn.prepareStatement("update Email_Config set last_uid =?,last_fetch_date =?  where mail_username=?");

            pstmt.setString(1, String.valueOf(lastUid));
            pstmt.setTimestamp(2, emailReceivedDate);
            pstmt.setString(3, mailUserName);
            pstmt.executeUpdate();
            logger.info("Statement executed successfully -- updateDataEmailConfig()");

            conn.close();

        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    conn.close();
            } catch (SQLException se) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

    }

    public void updateServerStatus(String status, EmailConfiguration emailConfiguration) {

        Connection conn = null;
        PreparedStatement pstmt = null;

        try {
            Class.forName(AppPropertyFields.getJdbcDriver());

            logger.info("Connecting to a selected database -- updateServerStatus()");
            conn = DriverManager.getConnection(AppPropertyFields.getDbUrl() + AppPropertyFields.getDbName()+TIME_ZONE, AppPropertyFields.getDbUser(), AppPropertyFields.getDbPwd());
            logger.info("Connected database successfully -- updateServerStatus()");
            pstmt = conn.prepareStatement("update Email_Config set mail_server_response_status = ?, mail_server_connection_date= ?," +
                    "date_updated= ? where mail_username= ?");
            java.util.Date utilDate = new java.util.Date();
            java.sql.Timestamp connectAndUpdateTime = new java.sql.Timestamp(utilDate.getTime());
            pstmt.setString(1, status);
            pstmt.setTimestamp(2, connectAndUpdateTime);
            pstmt.setTimestamp(3, connectAndUpdateTime);
            pstmt.setString(4, emailConfiguration.getMailUserName());
            pstmt.executeUpdate();
            logger.info("Statement executed successfully -- updateServerStatus()");
            conn.close();

        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    conn.close();
            } catch (SQLException se) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    public Map getHealthStatusList() {

        Connection conn = null;
        Statement stmt = null;
        Map healthStatusMap = new LinkedHashMap();

        try {
            Class.forName(AppPropertyFields.getJdbcDriver());

            logger.info("Connecting to a selected database -- getHealthStatusList()");

            conn = DriverManager.getConnection(AppPropertyFields.getDbUrl() + AppPropertyFields.getDbName()+TIME_ZONE, AppPropertyFields.getDbUser(), AppPropertyFields.getDbPwd());

            logger.info("Connected database successfully -- getHealthStatusList()");


            logger.info("Creating statement -- getEmailConfigData()");
            stmt = conn.createStatement();

            ResultSet rs = stmt.executeQuery(GET_HEALTH_STATUS_QUERY);

            logger.info("Statement executed successfully -- getHealthStatusList()");

            while (rs.next()) {

                Map emailConfiguration = new LinkedHashMap();

                emailConfiguration.put("mailConfig", rs.getString("mail_config"));
                emailConfiguration.put("lastUid", rs.getString("last_uid"));
                emailConfiguration.put("lastFetchDate", rs.getDate("last_fetch_date"));
                emailConfiguration.put("query", rs.getString("mail_db_post_query"));
                emailConfiguration.put("mailServerResponseStatus", rs.getString("mail_server_response_status"));
                emailConfiguration.put("mailServerConnectionDate", rs.getDate("mail_server_connection_date"));
                emailConfiguration.put("dateCreated", rs.getDate("date_created"));
                emailConfiguration.put("dateUpdated", rs.getDate("date_updated"));
                emailConfiguration.put("Status", rs.getString("status"));

                healthStatusMap.put(rs.getString("mail_username"), emailConfiguration);

            }
            rs.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException se) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

        return healthStatusMap;

    }
}

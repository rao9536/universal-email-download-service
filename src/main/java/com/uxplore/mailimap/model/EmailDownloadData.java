package com.uxplore.mailimap.model;

import java.sql.Blob;
import java.sql.Timestamp;

/**
 * @author Ramesh Rao
 */

public class EmailDownloadData {

    private String emailUID;
    private String emailFrom;
    private String emailTo;
    private String emailCC;
    private String emailBCC;
    private String emailSubject;
    private String emailBody;
    private String emailBodyHtml;
    private Blob emailAttachment;
    private String emailAttachmentType;
    private Timestamp emailDateReceived;
    private String emailHeaders;
    private String emailFlags;
    private String emailLabels;
    private Timestamp dateCreated;
    private Timestamp dateUpdated;
    private Timestamp dateCompleted;
    private String status;
    private long timeTaken;

    public String getEmailUID() {
        return emailUID;
    }

    public void setEmailUID(String emailUID) {
        this.emailUID = emailUID;
    }

    public String getEmailFrom() {
        return emailFrom;
    }

    public void setEmailFrom(String emailFrom) {
        this.emailFrom = emailFrom;
    }

    public String getEmailTo() {
        return emailTo;
    }

    public void setEmailTo(String emailTo) {
        this.emailTo = emailTo;
    }

    public String getEmailCC() {
        return emailCC;
    }

    public void setEmailCC(String emailCC) {
        this.emailCC = emailCC;
    }

    public String getEmailBCC() {
        return emailBCC;
    }

    public void setEmailBCC(String emailBCC) {
        this.emailBCC = emailBCC;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public String getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }

    public String getEmailBodyHtml() {
        return emailBodyHtml;
    }

    public void setEmailBodyHtml(String emailBodyHtml) {
        this.emailBodyHtml = emailBodyHtml;
    }

    public Blob getEmailAttachment() {
        return emailAttachment;
    }

    public void setEmailAttachment(Blob emailAttachment) {
        this.emailAttachment = emailAttachment;
    }

    public String getEmailAttachmentType() {
        return emailAttachmentType;
    }

    public void setEmailAttachmentType(String emailAttachmentType) {
        this.emailAttachmentType = emailAttachmentType;
    }

    public Timestamp getEmailDateReceived() {
        return emailDateReceived;
    }

    public void setEmailDateReceived(Timestamp emailDateReceived) {
        this.emailDateReceived = emailDateReceived;
    }

    public String getEmailHeaders() {
        return emailHeaders;
    }

    public void setEmailHeaders(String emailHeaders) {
        this.emailHeaders = emailHeaders;
    }

    public String getEmailFlags() {
        return emailFlags;
    }

    public void setEmailFlags(String emailFlags) {
        this.emailFlags = emailFlags;
    }

    public String getEmailLabels() {
        return emailLabels;
    }

    public void setEmailLabels(String emailLabels) {
        this.emailLabels = emailLabels;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Timestamp getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Timestamp dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public Timestamp getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(Timestamp dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(long timeTaken) {
        this.timeTaken = timeTaken;
    }
}

package com.uxplore.mailimap;

import com.uxplore.mailimap.model.AppPropertyFields;
import com.uxplore.mailimap.scheduler.EmailDownloadScheduler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @author Ramesh Rao
 */


@SpringBootApplication(scanBasePackages = {"com.uxplore.mailimap"})
public class MailimapApplication {

    public final static String QUARTZ_TIMER = "quartz.job.time";
    public final static String JDBC_DRIVER = "jdbc.driver";
    public final static String DB_URL = "db.url";
    public final static String DB_USER = "db.user";
    public final static String DB_PWD = "db.pwd";
    public final static String DB_NAME = "db.name";
    public final static String THREAD_TIMEOUT_VALUE = "thread.timeout.value";
    public final static String ATTACHMENTS_PATH = "attachments.path";

    @Resource
    private Environment env;

    public static void main(String[] args) {
        SpringApplication.run(MailimapApplication.class, args);
    }

    @PostConstruct
    public void init() {
        AppPropertyFields.setThreadTime(env.getRequiredProperty(QUARTZ_TIMER));
        AppPropertyFields.setJdbcDriver(env.getProperty(JDBC_DRIVER));
        AppPropertyFields.setDbUrl(env.getRequiredProperty(DB_URL));
        AppPropertyFields.setDbUser(env.getProperty(DB_USER));
        AppPropertyFields.setDbPwd(env.getProperty(DB_PWD));
        AppPropertyFields.setDbName(env.getProperty(DB_NAME));
        AppPropertyFields.setThreadTimeoutValue(Integer.parseInt(env.getProperty(THREAD_TIMEOUT_VALUE)));
        AppPropertyFields.setAttachmentsPath(env.getProperty(ATTACHMENTS_PATH));
        EmailDownloadScheduler emailDownloadScheduler = new EmailDownloadScheduler();
        emailDownloadScheduler.runEmailsScheduler();
    }

}

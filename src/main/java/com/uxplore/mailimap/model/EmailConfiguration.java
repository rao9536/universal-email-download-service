package com.uxplore.mailimap.model;

import java.util.Date;

/**
 * @author Ramesh Rao
 */


public class EmailConfiguration {
    private String accountName;
    private String mailServerHost;
    private int mailServerPort;
    private boolean mailServerSsl;
    private String mailServerMailbox;
    private String mailUserName;
    private String mailPassword;
    private String mailConfig;
    private String lastUid;
    private Date lastFetchDate;
    private String mailDbDatabase;
    private String mailDbTable;
    private String mailDbPostQuery;
    private String mailServerResponseStatus;
    private Date mailServerConnectionDate;
    private String metadata;
    private Date dateCreated;
    private Date dateUpdated;
    private String status;
    private Date dateCompleted;
    private int timeTaken;
    private String bashExecution;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getMailServerHost() {
        return mailServerHost;
    }

    public void setMailServerHost(String mailServerHost) {
        this.mailServerHost = mailServerHost;
    }

    public int getMailServerPort() {
        return mailServerPort;
    }

    public void setMailServerPort(int mailServerPort) {
        this.mailServerPort = mailServerPort;
    }

    public boolean isMailServerSsl() {
        return mailServerSsl;
    }

    public void setMailServerSsl(boolean mailServerSsl) {
        this.mailServerSsl = mailServerSsl;
    }

    public String getMailServerMailbox() {
        return mailServerMailbox;
    }

    public void setMailServerMailbox(String mailServerMailbox) {
        this.mailServerMailbox = mailServerMailbox;
    }

    public String getMailUserName() {
        return mailUserName;
    }

    public void setMailUserName(String mailUserName) {
        this.mailUserName = mailUserName;
    }

    public String getMailPassword() {
        return mailPassword;
    }

    public void setMailPassword(String mailPassword) {
        this.mailPassword = mailPassword;
    }

    public String getMailConfig() {
        return mailConfig;
    }

    public void setMailConfig(String mailConfig) {
        this.mailConfig = mailConfig;
    }

    public String getLastUid() {
        return lastUid;
    }

    public void setLastUid(String lastUid) {
        this.lastUid = lastUid;
    }

    public Date getLastFetchDate() {
        return lastFetchDate;
    }

    public void setLastFetchDate(Date lastFetchDate) {
        this.lastFetchDate = lastFetchDate;
    }

    public String getMailDbDatabase() {
        return mailDbDatabase;
    }

    public void setMailDbDatabase(String mailDbDatabase) {
        this.mailDbDatabase = mailDbDatabase;
    }

    public String getMailDbTable() {
        return mailDbTable;
    }

    public void setMailDbTable(String mailDbTable) {
        this.mailDbTable = mailDbTable;
    }

    public String getMailDbPostQuery() {
        return mailDbPostQuery;
    }

    public void setMailDbPostQuery(String mailDbPostQuery) {
        this.mailDbPostQuery = mailDbPostQuery;
    }

    public String getMailServerResponseStatus() {
        return mailServerResponseStatus;
    }

    public void setMailServerResponseStatus(String mailServerResponseStatus) {
        this.mailServerResponseStatus = mailServerResponseStatus;
    }

    public Date getMailServerConnectionDate() {
        return mailServerConnectionDate;
    }

    public void setMailServerConnectionDate(Date mailServerConnectionDate) {
        this.mailServerConnectionDate = mailServerConnectionDate;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(Date dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public int getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(int timeTaken) {
        this.timeTaken = timeTaken;
    }

    public String getBashExecution() {
        return bashExecution;
    }

    public void setBashExecution(String bashExecution) {
        this.bashExecution = bashExecution;
    }
}

package com.uxplore.mailimap.controller;


import com.sun.mail.imap.IMAPFolder;
import com.uxplore.mailimap.model.EmailConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.*;
import javax.mail.event.MessageCountEvent;
import javax.mail.event.MessageCountListener;


public class EmailListener implements MessageCountListener {

    private static final Logger logger = LoggerFactory.getLogger(EmailListener.class);
    public EmailConfiguration emailConfiguration;
    public Store store;
    public UIDFolder uidFolder;
    public IMAPFolder imapFolder;

    public EmailListener(EmailConfiguration emailConfiguration, Store store, UIDFolder uf, IMAPFolder imapFolder) {
        this.emailConfiguration = emailConfiguration;
        this.store = store;
        this.uidFolder = uf;
        this.imapFolder = imapFolder;
    }

    @Override
    public void messagesAdded(final MessageCountEvent event) {
        logger.info("Starting auto sensing of " + emailConfiguration.getMailUserName());
        Message[] messages = event.getMessages();
        try {
            CheckEmailController checkEmailController = new CheckEmailController(emailConfiguration);
            String response = checkEmailController.processAndSaveMsgs(messages, uidFolder, null, imapFolder);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void messagesRemoved(MessageCountEvent messageCountEvent) {

    }
}

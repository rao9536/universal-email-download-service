package com.uxplore.mailimap.model;

/**
 * @author Ramesh Rao
 */


public class AppPropertyFields {
    private static String threadTime;
    private static String jdbcDriver;
    private static String dbUrl;
    private static String dbUser;
    private static String dbPwd;
    private static String dbName;
    private static int threadTimeoutValue;
    private static  String attachmentsPath;

    public static String getThreadTime() {
        return threadTime;
    }

    public static void setThreadTime(String threadTime) {
        AppPropertyFields.threadTime = threadTime;
    }

    public static String getJdbcDriver() {
        return jdbcDriver;
    }

    public static void setJdbcDriver(String jdbcDriver) {
        AppPropertyFields.jdbcDriver = jdbcDriver;
    }

    public static String getDbUrl() {
        return dbUrl;
    }

    public static void setDbUrl(String dbUrl) {
        AppPropertyFields.dbUrl = dbUrl;
    }

    public static String getDbUser() {
        return dbUser;
    }

    public static void setDbUser(String dbUser) {
        AppPropertyFields.dbUser = dbUser;
    }

    public static String getDbPwd() {
        return dbPwd;
    }

    public static void setDbPwd(String dbPwd) {
        AppPropertyFields.dbPwd = dbPwd;
    }

    public static String getDbName() {
        return dbName;
    }

    public static void setDbName(String dbName) {
        AppPropertyFields.dbName = dbName;
    }

    public static int getThreadTimeoutValue() {
        return threadTimeoutValue;
    }

    public static void setThreadTimeoutValue(int threadTimeoutValue) {
        AppPropertyFields.threadTimeoutValue = threadTimeoutValue;
    }

    public static String getAttachmentsPath() {
        return attachmentsPath;
    }

    public static void setAttachmentsPath(String attachmentsPath) {
        AppPropertyFields.attachmentsPath = attachmentsPath;
    }
}

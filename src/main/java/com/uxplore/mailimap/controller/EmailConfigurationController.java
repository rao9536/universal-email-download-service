package com.uxplore.mailimap.controller;

import com.uxplore.mailimap.DbConnections.EmailConfigJdbc;
import com.uxplore.mailimap.model.EmailConfiguration;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;


/**
 * @author Ramesh Rao
 */

public class EmailConfigurationController implements Job {

    private static final Logger logger = LoggerFactory.getLogger(EmailConfigurationController.class);

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        EmailConfigJdbc emailConfigJdbc = new EmailConfigJdbc();

        logger.info("cron job -- getting list of config data list !!");
        List emailConfigList = emailConfigJdbc.getEmailConfigData();

        for (int i = 0; i <= emailConfigList.size() - 1; i++) {
            EmailConfiguration emailConfiguration = (EmailConfiguration) emailConfigList.get(i);
            try {

                logger.info("cron job -- starting independent thread for every config data !!");

                boolean isThreadExists = false;
                Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
                for (Thread thread : threadSet) {
                    if ((thread.getName().equalsIgnoreCase(emailConfiguration.getMailUserName()))) {
                        if (!thread.isInterrupted()) {
                            logger.info(emailConfiguration.getMailUserName() + " Same thread is alive, so not starting thread again");
                            isThreadExists = true;
                            break;
                        }
                    }
                }
                if (!isThreadExists) {
                    Thread emailThread = new Thread(new CheckEmailController(emailConfiguration));
                    emailThread.start();
                    emailThread.setName(emailConfiguration.getMailUserName());
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

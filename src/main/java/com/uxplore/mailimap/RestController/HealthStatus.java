package com.uxplore.mailimap.RestController;

import com.uxplore.mailimap.DbConnections.EmailConfigJdbc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author Ramesh Rao
 */

@RestController
public class HealthStatus {
    private static final Logger logger = LoggerFactory.getLogger(HealthStatus.class);

    @RequestMapping(value = {"/health"}, method = RequestMethod.GET, produces = "application/json;")
    public @ResponseBody
    Object getHealthMonitorStatus() {
        logger.info("health status api triggered");
        EmailConfigJdbc emailConfigJdbc = new EmailConfigJdbc();
        Map<String, Object> healthStatusMap = emailConfigJdbc.getHealthStatusList();
        return healthStatusMap;
    }
}
